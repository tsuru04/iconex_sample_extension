import React,{useState} from 'react';
import IconService from 'icon-sdk-js';

function HomePage(){
    const [value,setValue]= useState('');
    const[disable, setDisable]= useState(true);
    var payload = "100000000";
    var fromAddress= payload
    var requestScoreForm = document.getElementById("request-score-form")
    var scoreData= document.getElementById("score-data")
    var responseHasAccount = document.getElementById("response-has-account")
    var responseHasAddress = document.getElementById("response-has-address")
    var responseAddress= document.getElementById("response-address")
    var responseScore= document.getElementById("response-score")
    var jsonRpc0 = document.getElementById("json-rpc-0")
    var jsonRpc1 = document.getElementById("json-rpc-1")
    var jsonRpc2 = document.getElementById("json-rpc-2")
    var jsonRpc3 = document.getElementById("json-rpc-3")
    var signingData = document.getElementById("signing-data")
    var responseSigning = document.getElementById("response-signing")
    
    const setRequestScoreForm=function(e){
        // e.preventDefault();
        const {IconBuilder,IconAmount,IconConverter} = IconService;
        console.log("radio button clicked")
        switch (value){
            case 'read-only':
                console.log("read-only selected");
                var callBuilder= new IconBuilder.CallBuilder;
                var readOnlyData= callBuilder
                    .from(fromAddress)
                    .to('cx43f59485bd34d0c7e9312835d65cb399f6d29651')
                    .method("hello")
                    .build()
                scoreData.data= JSON.stringify({"jsonrpc": "2.0",
                        "method": "icx_call",
                        "params": readOnlyData,
                        "id": 50889})
                        console.log("worked")
                break;
            case 'send-transaction':
                console.log("send transaction selected")
                var callTransactionBuilder= new IconBuilder.CallTransactionBuilder
                var callTransactionData = callTransactionBuilder 
                        .from(fromAddress)
                        .to("cxb20b5ff06ba50aef42c7832958af59f9ae0651e7")
                        .nid(IconConverter.toBigNumber(3))
                        .timestamp((new Date()).getTime() * 1000)
                        .stepLimit(IconConverter.toBigNumber(1000000))
                        .version(IconConverter.toBigNumber(3))
                        .method('createToken')
                        .params({
                            "price": IconConverter.toHex(10000),
                            "tokenType": IconConverter.toHex(2)
                        })
                        .build()
                scoreData.data= JSON.stringify({
                    "jsonrpc": "2.0",
                    "method": "icx_sendTransaction",
                    "params": IconConverter.toRawTransaction(callTransactionData),
                    "id": 50889
                })
                console.log("worked")
                break;
            case 'icx transfer':
                console.log("icx transfer selected")
                var icxTransactionBuilder= new IconBuilder.IcxTransactionBuilder
                var icxTransferData= icxTransactionBuilder
                        .from(fromAddress)
                        .to("hx04d669879227bb24fc32312c408b0d5503362ef0")
                        .nid(IconConverter.toBigNumber(3))
                        .value(IconAmount.of(1, IconAmount.Unit.ICX).toLoop())
                        .timestamp((new Date()).getTime() * 1000)
                        .version(IconConverter.toBigNumber(3))
                        .stepLimit(IconConverter.toBigNumber(100000))
                        .build();
                    scoreData.data =JSON.stringify({
                        "jsonrpc": "2.0",
                        "method": "icx_sendTransaction",
                        "params": IconConverter.toRawTransaction(icxTransferData),
                        "id": 50889
                    })
                    console.log("worked")
                break;
            default:
                console.log("custom")

    }}
    
    
    const eventHandler= function(e){
        // e.preventDefault();
        console.log("clicked")
        
            switch(value){
                case "REQUEST_HAS_ACCOUNT":
                    console.log("works")
                    responseHasAccount.innerHTML = "> Result :" +payload;
                    break;
                case "REQUEST_HAS_ADDRESS":
                    console.log("request has address");
                    responseHasAddress.innerHTML= "> Resul:" + payload
                    break;
                case "HAS_ADDRESS":
                    console.log("has address");
                    responseAddress.innerHTML= "> Selected Address :"+payload;
                    // jsonRpc0.disable = setDisable(false)
                    // // jsonRpc1.setDisable
                    // // jsonRpc2.setDisable
                    // // jsonRpc3.setDisable
                    break;
                case "RESPONSE_JSON_RPC":
                    console.log("request_json_rpc")
                    responseScore.data= JSON.stringify(payload);
                    break;
                case "CANCEL_JSON-RPC":
                    responseScore.data = null;
                    break;
                case "REQUEST_SIGNING":
                    console.log("request_signing")
                    signingData.data = null
                    responseSigning.data = ">Signature :"
                    responseSigning.innerHTML="> Signature : "+ signingData;
                    break;
                case "CANCEL_SIGNING":
                    signingData.data = null
                    responseSigning.data = "> Signature : ";
                    break
                default:
                    console.log("nope")
            }


        }
    return (

        <div className="ui container">
            <h1>ICONEX SAMPLE EXTENSION</h1>
            <p>-------------------------</p>
            <article>
            <h4>Has Account : Requests for whether iconex has any icon wallet</h4>
            <button className="ui inverted primary button " onClick={()=>eventHandler(setValue("REQUEST_HAS_ACCOUNT"))}>REQUEST_HAS_ACCOUNT</button>
            <p id="response-has-account">Result : {value} {payload}</p>
            </article>

            <p>--------------------------</p>
            <article>
            <h4>Has Address : Requests for whether iconex has any icon wallet address</h4>
            <button className="ui inverted primary button " onClick={()=> eventHandler(setValue("REQUEST_HAS_ADDRESS"))}>REQUEST_HAS_ADDRESS</button>
            <p id="response-has-address">Result :</p>
            </article>

            <p>--------------------------</p>
            <article>
            <h4>Address : Request for the address to use for service</h4>
            <button className="ui inverted primary button " onClick={()=>eventHandler(setValue("HAS_ADDRESS"))}>SELECTED_ADDRESS</button>
            <p id="response-address">Selected Address :</p>
            </article>

            <p>---------------------------</p>
            <article>
                <h4>JSON-RPC:Request for calling standard ICON JSON-RPC</h4>
                <p style={{color: "red"}}>User Confirmation Needed</p>
                <form className="ui form" id="request-score-form">
                    <input type="radio" id="json-rpc-0" disabled={disable} onClick={()=>setRequestScoreForm(setValue("custom"))} data="custom"></input>
                    <label>Custom</label>
                    <br/>
                    <input type="radio" disabled={disable} id="json-rpc-1" onClick={()=>setRequestScoreForm(setValue("read-only"))}data="read-only"></input>
                    <label>Read-ONLY</label><br/>
                    <input type="radio" id="json-rpc-2" disabled={disable} onClick={()=>setRequestScoreForm(setValue("send-transaction"))} data="send transaction"></input>
                    <label>Send transaction</label><br/>
                    <input type="radio" id = "json-rpc-3" disabled={disable} onClick={()=>setRequestScoreForm(setValue("icx transfer"))} data ="icx transfer"></input>
                    <label>ICX Transfer</label><br/>
                </form>
            </article>

            <p>------------------------------------</p>
            <article>
                <h3>Param Data:</h3><br/>
                <textarea rows="10" id="score-data"></textarea><br/>
                <button className="ui inverted primary button" onClick={()=>eventHandler(setValue("RESPONSE_JSON_RPC"))}>REQUEST_JSON_RPC</button> <br/>
                <p>Result: {value}</p> <textarea id="response-score" rows="10"/>
            </article>
            
            <p>------------------------------------</p>
            <article>
                 <h4>Signing : Request for only signing tx hash. (User confirmation is always required.)</h4>
                 <h4>Tx Hash : <input id="signing-data" type="text" placeholder="9babe5d2911e8e42dfad72a589202767f95c6fab49523cdc16279a7b8f82eab2" /></h4>
                 <button className="ui inverted primary button" onClick={()=>eventHandler(setValue("REQUEST_SIGNING"))}>REQUEST_SIGNING</button>
                 <p id="response-signing"> Signature :</p>
            </article>
        </div>
    )

    
}


export default HomePage;
